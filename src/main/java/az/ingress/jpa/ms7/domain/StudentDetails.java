package az.ingress.jpa.ms7.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity
@Data
@ToString(exclude = "student")
public class StudentDetails {

    @Id
    @Column(name = "student_id")
    private Long id;

    private String about;

    @OneToOne
    @MapsId
    @JoinColumn(name = "student_id")
    private Student student;


}
