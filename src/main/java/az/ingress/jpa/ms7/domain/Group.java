package az.ingress.jpa.ms7.domain;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Data
//@ToString(exclude = "students")
@Table(name = "s_groups")
@NamedEntityGraph(
        name = "graph.GroupClasses",
        attributeNodes = @NamedAttributeNode(value = "students", subgraph = "subgraph.classes"),
        subgraphs = {@NamedSubgraph(name = "subgraph.classes",
                attributeNodes = @NamedAttributeNode(value = "classes"))})
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    /// @Fetch(FetchMode.JOIN)
    private Set<Student> students; //field
}
