package az.ingress.jpa.ms7;

import az.ingress.jpa.ms7.domain.Student;
import az.ingress.jpa.ms7.domain.Student$;
import az.ingress.jpa.ms7.repository.StudentRepository;
import az.ingress.jpa.ms7.services.TransferService;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.persistence.EntityManagerFactory;

//@EnableCaching
@SpringBootApplication
@RequiredArgsConstructor
public class Ms7Application implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final TransferService transferService;

    public static void main(String[] args) {
        SpringApplication.run(Ms7Application.class, args);
    }

    // L1 - EM Cache - Weak
    // L2 - EMF Cache - Soft
    // 3d party dependency

    @Override
    public void run(String... args) throws Exception {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                transferService.transferWithAnnotationT1(200.0);
            }
        };

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                transferService.transferWithAnnotationT2(200.0);
            }
        };
//
//        Thread thread = new Thread(runnable);
//        thread.start();
////
//       // Thread.sleep(10000);
//        Thread thread1 = new Thread(runnable2);
//        thread1.start();
    }


//    Group group = new Group();
//        group.setName("MS8");
//
//    Classes classes = new Classes();
//        classes.setName("MS8");
//
//    Classes classes2 = new Classes();
//        classes2.setName("MS9");
//
//    Student student = new Student();
//        student.setName("Test");
//        student.setClasses(Set.of(classes));
//        classes.setStudent(student);
//        student.setGroup(group);
//
//    Student student3 = new Student();
//        student3.setName("Test2");
//        student3.setClasses(Set.of(classes2));
//        classes2.setStudent(student3);
//        student3.setGroup(group);
//
//        group.setStudents(Set.of(student, student3));
//
//        groupsRepository.save(group);
//        System.out.println("id is" + group.getId());
//
//
//    EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//
//
//    EntityGraph<?> entityGraph = entityManager.createEntityGraph("graph.GroupClasses");
//
//    group = entityManager.find(Group.class, group.getId(), Collections.singletonMap("javax.persistence.loadgraph", entityGraph));
//        System.out.println(group);
//
//        entityManager.close();
    //createStudents();

//        StudentDetails studentDetails = studentDetailsRepository.findById(1L)
//                .get();

    //  System.out.println(studentDetails.getStudent());

//        Student student = new Student();
//        student.setAge(20);
//        student.setName("TestName");
//        student.setEmail("test@test.com");
//
//        StudentDetails studentDetails = new StudentDetails();
//        studentDetails.setStudent(student);
//        studentDetails.setAbout("Test Test");
//
//        student.setStudentDetails(studentDetails);
//
//        studentRepository.save(student);
//
    //  studentRepository.findById(4L)
    //        .ifPresent(System.out::println); //


    //getSTudentsWithJpaStreamer();

    //        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
//
//        studentRepository.saveAll(EnhancedRandom.randomListOf(10, Student.class));
//
//        jpaStreamer
//                .stream(Student.class)
//                .filter(Student$.name.startsWith(""))
//                .sorted(Student$.age.reversed().thenComparing(Student$.name.comparator()))
//                .skip(0)
//                .limit(5)
//                .forEach(System.out::println);

    private void getSTudentsWithJpaStreamer() {
        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);

        jpaStreamer
                .stream(Student.class)
                .filter(Student$.age.greaterOrEqual(10))
                .filter(Student$.name.startsWith("Test"))
                .filter(Student$.id.isNotNull())
                .skip(5)
                .limit(10)
                .forEach(System.out::println);
    }

    private void createStudents() {
        for (int i = 0; i < 10; i++) {
            Student student = new Student();
            student.setName("Test " + i);
            student.setEmail("test@test.com");
            student.setAge(13 + i);
            studentRepository.save(student);
        }

        //select student0_.id as id1_0_, student0_.age as age2_0_, student0_.email as email3_0_, student0_.name as name4_0_ from students student0_ where student0_.age>? and student0_.name=?

        studentRepository.findByAgeQuerySQL(18, "Test 9")
                .stream()
                .forEach(System.out::println);
    }

    //        //Java Persistence API (JPA)
//        EntityManagerFactory entityManagerFactory;
//        EntityManager entityManager;
//
//        //Hibernate
//        SessionFactory hibernateSessionFactory;
//        SessionFactoryImpl sessionFactoryImpl;
//
//        //Hibernate
//        Session session;
//        SessionImpl sessionImpl;


//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        System.out.println("Entity manager factory: " + entityManagerFactory.unwrap(EntityManagerFactory.class));
//        System.out.println("Entity manager : " + entityManager.unwrap(EntityManager.class));
//
}
