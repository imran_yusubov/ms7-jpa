package az.ingress.jpa.ms7.controller;

import az.ingress.jpa.ms7.domain.Account;
import az.ingress.jpa.ms7.services.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/transfer")
public class TransferController {

    private final TransferService transferService;

    @PostMapping
    public Account transfer() {
        return transferService.transferWithAnnotationT1(200.0);
    }

}
