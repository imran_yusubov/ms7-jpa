package az.ingress.jpa.ms7.services;

import az.ingress.jpa.ms7.domain.Account;
import az.ingress.jpa.ms7.dto.AccountDto;
import az.ingress.jpa.ms7.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.lang.ref.PhantomReference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final ModelMapper mapper;
    // 200 MB heap, 500 mb account info - OOM
    private Map<Long, Account> accountMap = new HashMap<>(); // 1 mln
    //Reference Types
    String s = "Test"; //4 reference types, s=null
    WeakReference<String> weakReference = new WeakReference<>("Test"); //
    SoftReference<String> stringSoftReference = new SoftReference<>("Test2");//
    PhantomReference<String> phantomReference = new PhantomReference<>("Test", null);

    // Locks
    // ACID -> Atomicity Consistency Isolation Durability
    // Isolation -> Read Uncommied Read Commited RR Serializable
    // Dirty Read -> RU,
    // PR -> serializable
    // NRR-> un-commied commited
    // @Transactional -> EM -> open managed, perist
    // repo.save(); // -> finally commit, flush
    // no rollback, rollback for,
    // does not work within the same class, CG Lib, AspectJ
    // L1 EM (Weak), L2 EMF (Soft)
    // Strong, Weak, Soft, Phantom
    // EM, EMF, SessionImp, SessionFactoryImpl.
    // BeanAwerLocal -> homework
    // findAll -> why not cached?
    // Phantom reference -> why it was not found in ref queue
    // Readonly , homework AspectJ

    // Level 2 cache enabled


    @Transactional
    public void find() throws InterruptedException {
        Account account = accountRepository.findById(1L).get();
        System.out.println("Start update");
        Thread.sleep(10000);
        accountRepository.findById(1L).get();
        if (account.getBalance() >= 200) {
            System.out.println("Yes we can proceeed");
        } else {
            System.out.println("No we can't ");
        }
    }

    public List<AccountDto> getAccounts() {
        return accountRepository.findAll()
                .stream()
                .map(account -> mapper.map(account,AccountDto.class))
                .collect(Collectors.toList());
    }

    @Cacheable(value = "accounts", key = "#id") //2000
    public AccountDto getAccount(Long id) {
        return accountRepository.findById(id)
                .map((a) -> mapper.map(a, AccountDto.class))
                .orElseThrow(() -> new RuntimeException("Not found"));
    }

    @CacheEvict(value = "accounts", key = "#id")  //5 san
    public AccountDto update(Long id, AccountDto accountDto) {
        log.info("Updating account");
        Account account = mapper.map(accountDto, Account.class);
        account.setId(id);
        return mapper.map(accountRepository.save(account), AccountDto.class);
    }

    @CacheEvict(value = "accounts", allEntries = true)
    public void delete(Long id) {
        accountRepository.findById(id)
                .ifPresent(account -> {
                    accountRepository.delete(account);
                });
    }
}
