package az.ingress.jpa.ms7.services;

import az.ingress.jpa.ms7.domain.Student;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Slf4j
@RequiredArgsConstructor
public class StudentService {

    private final EntityManagerFactory entityManagerFactory;

    public void createStudent(Student student) {
        log.info("Creating student");
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(student);
            entityManager.getTransaction().commit();

        } finally {
            entityManager.close();
        }
    }


}
