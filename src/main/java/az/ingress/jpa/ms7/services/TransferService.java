package az.ingress.jpa.ms7.services;

import az.ingress.jpa.ms7.domain.Account;
import az.ingress.jpa.ms7.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    @SneakyThrows
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Account transferWithAnnotationT1(Double amount) {
        log.trace("Thread id is {}", Thread.currentThread().getId());

        Account source = accountRepository.findById(1L).get();
        if (amount > source.getBalance())
            throw new RuntimeException("Insufficient amount in the account, required : " + amount + " available: " + source.getBalance());
        log.trace("Thread id retrieved account 1 {} {} ", Thread.currentThread().getId(), source.getBalance());
        Thread.sleep(10000);
        Account target = accountRepository.findById(3L).get();
        log.trace("Thread id retrieved account 3 {} {} ", Thread.currentThread().getId(), target.getBalance());

        source.setBalance(source.getBalance() - amount);
        target.setBalance(target.getBalance() + amount);

        accountRepository.save(source);
        accountRepository.save(target);
        log.trace("Thread completed {}", Thread.currentThread().getId());
        return target;
    }


    @SneakyThrows
    @Transactional
    public Account transferWithAnnotationT2(Double amount) {
        log.trace("Thread id is {}", Thread.currentThread().getId());

        Account source = accountRepository.findById(1L).get();
        if (amount > source.getBalance())
            throw new RuntimeException("Insufficient amount in the account, required : " + amount + " available: " + source.getBalance());
        log.trace("Thread id retrieved account 1 {} {} ", Thread.currentThread().getId(), source.getBalance());
        Thread.sleep(20000);
        Account target = accountRepository.findById(3L).get();
        log.trace("Thread id retrieved account 3 {} {} ", Thread.currentThread().getId(), target.getBalance());

        source.setBalance(source.getBalance() - amount);
        target.setBalance(target.getBalance() + amount);

        accountRepository.save(source);
        accountRepository.save(target);
        log.trace("Thread completed {}", Thread.currentThread().getId());
        return target;
    }

    public void transferTransactional(Double amount) throws Exception {
        entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            Account source = entityManager.find(Account.class, 1L);
            Account target = entityManager.find(Account.class, 2L);
            transfer(source, target, amount);
        } catch (RuntimeException e) { //
            transaction.rollback();
            throw e;
        } finally {
            if (transaction.isActive())
                transaction.commit();
            entityManager.close();
        }
    }


    @SneakyThrows
    public void transfer(Account source, Account target, Double amount) {
        if (source.getBalance() < amount) {
            throw new RuntimeException("Insufficient balance");
        }
        System.out.println("Account balance is :" + source.getBalance() + " transefring...");
        source.setBalance(source.getBalance() - amount); //
        target.setBalance(target.getBalance() + amount);
        entityManager.flush();
        Thread.sleep(30000);
        if (true)
            throw new Exception();
    }
}
