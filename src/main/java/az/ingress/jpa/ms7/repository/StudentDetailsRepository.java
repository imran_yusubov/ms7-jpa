package az.ingress.jpa.ms7.repository;

import az.ingress.jpa.ms7.domain.StudentDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDetailsRepository extends JpaRepository<StudentDetails, Long> {
}
