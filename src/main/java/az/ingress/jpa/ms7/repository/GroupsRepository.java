package az.ingress.jpa.ms7.repository;

import az.ingress.jpa.ms7.domain.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface GroupsRepository extends JpaRepository<Group, Long> {

    @Query(value =
            "Select DISTINCT g from Group g JOIN FETCH g.students s")
    Set<Group> getALlGroupsWithStudents();


//    @EntityGraph(
//            value = "Group.students",
//            type = EntityGraph.EntityGraphType.LOAD
//    )
//    List<Group> getALlGroupsWithStudentsEg();
}
